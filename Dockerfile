FROM golang:latest
COPY ./app /go/src/bitbucket.org/lyledean/palindromes/app
WORKDIR /go/src/bitbucket.org/lyledean/palindromes/app
RUN go get ./
RUN go build -o main .
RUN go test .
CMD ["/go/src/bitbucket.org/lyledean/palindromes/app/main"]