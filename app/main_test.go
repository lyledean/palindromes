package main

import (
	"testing"
	"time"
)


//TEST POP AND INSERT 
func TestPopAndInsert(t *testing.T) {
	one := Palindrome{Value: "one", Time_parsed: time.Now()}
	two := Palindrome{Value: "two", Time_parsed: time.Now()}
	three := Palindrome{Value: "three", Time_parsed: time.Now()}
	four := Palindrome{Value: "four", Time_parsed: time.Now()}
	pals := []Palindrome{one, two, three}
	popped := PopAndInsert(four, pals)
	if (len(popped) != 3) {
		t.Errorf("The array is not the correct size")
	}
	if (popped[0] != two) {
		t.Errorf("Incorrect assignment")
	}
	if (popped[2] != four) {
		t.Errorf("Incorrect assignment")
	}

}

///TEST FOR TIME INTERVALS
func TestCheckTimeIntervalTrue(t *testing.T) {
	pal := Palindrome{Time_parsed: time.Now(), Value: "test"}
	truth := CheckTimeInterval(pal)
	if (truth == false) {
		t.Errorf("Check time interval is false when it should be true")
	}
}

func TestCheckTimeIntervalJustLessThan10Minutes(t *testing.T) {
	now := time.Now()
	then := now.Add(time.Duration(-599999) * time.Millisecond)
	pal := Palindrome{Time_parsed: then, Value: "test"}
	truth := CheckTimeInterval(pal)
	if (truth == false) {
		t.Errorf("Check time interval is false when it should be true")
	}
}

func TestCheckTimeIntervalMoreThan10Minutes(t *testing.T) {
	now := time.Now()
	then := now.Add(time.Duration(-600001) * time.Millisecond)
	pal := Palindrome{Time_parsed: then, Value: "test"}
	truth := CheckTimeInterval(pal)
	if (truth == true) {
		t.Errorf("Check time interval is true when it should be false")
	}

}

//TEST FOR PARSING PALINDROMES
func TestRemoveNonAlphaNumberCharacterAndLower(t *testing.T) {
	s := "A man, a plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!"
	parsedString := RemoveNonAlphaNumericCharactersAndLower(s)
	if (parsedString != "amanaplanacatahamayakayamahatacanalpanama") {
		t.Errorf("Palindrome %s is incorrect", parsedString)
	}

}

func TestIsPalindrome_SimpleString(t *testing.T){
	examples := []string{"Anna","Kayak","Madam","Mom","Noon","Radar","Refer","Rotor","Wow"}
	
	for _,s := range examples {
		if(IsPalindrome(s) == false) {
			t.Errorf("Palindrome %s should be true", s)
		}
	}
}

func TestIsNotPalindrome_SimpleString(t *testing.T){
	examples := []string{"dAnna","dayak","ladam","tom","zoon","dadar","defer","aRotor","aWow"}
	
	for _,s := range examples {
		if(IsPalindrome(s) == true) {
			t.Errorf("Palindrome %s should be false", s)
		}
	}
}

func TestMultiWordPalindromesTrue(t *testing.T){
	examples := []string{"Dammit I'm Mad","A man, a plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!", "Amy, must I jujitsu my ma?"}

	for _,s := range examples {
		if(IsPalindrome(s) == false) {
			t.Errorf("Palindrome %s should be true", s)
		}
	}
}


func TestMultiWordPalindromesFalse(t *testing.T){
	examples := []string{"A banana, no plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!", "Amy, must I jujitsu my mannn?"}

	for _,s := range examples {
		if(IsPalindrome(s) == true) {
			t.Errorf("Palindrome %s should be false", s)
		}
	}
}
