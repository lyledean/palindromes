package main

import (
	"fmt"
	"log"
	"io/ioutil"
	"net/http"
	"strings"
	"regexp"
	"time"
	"encoding/json"

)

type Palindrome struct {
	Time_parsed time.Time `json:"time_parsed"`
	Value string `json:"value"`
}

type Message struct {
	Palindrome string `json:"palindrome"`
}


//SET AS GLOBAL VARIABLES 
var Palindromes []Palindrome
var interval = 600000 * time.Millisecond //10 minutes

func RespondWithJson(w http.ResponseWriter, code int, object interface{}) {
	response, err := json.Marshal(object)
	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}


func PopAndInsert(pal Palindrome, pals []Palindrome) []Palindrome {
	//append new value to end of array and remove first value
	return append(pals[1:], pal)
}

func InsertPalindrome (s string) {
	//limit length of array to 10
	new_value := Palindrome{Time_parsed: time.Now(), Value: s}
	if (len(Palindromes) == 10) {
		//keep palindrome array at 10
		//remove first value and append to new value to end of array
		Palindromes = PopAndInsert(new_value, Palindromes)
	}
	//less than 10 we will just append new value to end of array
	if (len(Palindromes) < 10 ) {
		Palindromes = append(Palindromes, new_value)
	}
}

func CheckTimeInterval(pal Palindrome) bool {
	if (time.Since(pal.Time_parsed) < interval) {
		return true
	}
	return false 
}


func GetPalindromesForTheLast10Minutes() []Palindrome {
	//just loop through global Palindromes variable - check if each timestamp is within 10 minutes and return as a new object
	pals := []Palindrome{}
	for _, pal := range Palindromes {
		//only append if time difference is 10 minutes
		if (CheckTimeInterval(pal)) {
			pals = append(pals, pal)
		}
	}
	return pals

}

func RemoveNonAlphaNumericCharactersAndLower(s string) string {
	s = strings.ToLower(s)
	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
    if err != nil {
        log.Fatal(err)
    }
    parsedString := reg.ReplaceAllString(s, "")
	return parsedString
}

func IsPalindrome(s string) bool {
	s = RemoveNonAlphaNumericCharactersAndLower(s)
	mid := len(s) / 2
	last := len(s) - 1
	for i := 0; i < mid; i++ {
	 if s[i] != s[last-i] {
	  return false
	 }
	}
	return true
   }


func main() {
	http.HandleFunc("/palindromes", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "GET" {
			RespondWithJson(w, http.StatusCreated, GetPalindromesForTheLast10Minutes())
		}
		if r.Method == "POST" {
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				panic(err)
			}
			defer r.Body.Close()
			var msg Message
			err = json.Unmarshal(body, &msg)
			if err != nil {

			}
			if (IsPalindrome(msg.Palindrome)) {
				InsertPalindrome(msg.Palindrome)
				fmt.Fprintf(w, "true")
			} else {
				fmt.Fprintf(w, "false")
			}
		}
	})
	log.Fatal(http.ListenAndServe(":8000", nil))
}