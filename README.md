##To run 


BUILD IMAGE
> docker build -t palindrome .

RUN IMAGE
> docker run -p 8000:8000 -it palindrome


##Tests

These are run inside the Dockerfile on build, in the main_test.go file.

##To add data
GET

> curl localhost:8000/palindromes

POST 

> curl localhost:8000/palindromes -d '{"palindrome": "anna"}'


##To do

- Add further tests for http get and post requests
